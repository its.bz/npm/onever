# onever :: One version tool

A simple tool for NodeJS projects to manage single version of all modules.

## Installation

```shell
yarn add itsbz-onever
```

or

```shell
npm install itsbz-onever
```

## Usage

```shell
itsbz-onever --help
itsbz-onever init
itsbz-onever get
itsbz-onever set 1.0.1
itsbz-onever inc patch
itsbz-onever inc minor
itsbz-onever inc major
itsbz-onever apply
```

## Output sample
![screen_console](https://gitlab.com/its.bz/npm/onever/-/raw/master/screen_console.png)

## Contributing

0. Create project directory: `mkdir itsbz-onever && cd $_`
1. Clone repo: `git clone https://gitlab.com/its.bz/npm/onever.git ./`
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Bugs
Submit bugs [here](https://gitlab.com/its.bz/npm/onever/-/issues)

## History
```
1.0.19 @ 2 February 2025  
- Small refactoring  
- Upgrade app version

1.0.18 @ 2 February 2025  
- Small refactoring  
- Upgrade app version

1.0.17 @ 2 February 2025  
- Upgrade app version

1.0.16 @ 2 February 2025  
- Update all packages  
- Upgrade app version

1.0.15 @ 29 March 2023  
- Upgrade app version  
- Update publish script

1.0.14 @ 29 March 2023  
- Update publish script  
- Upgrade app version  
- Update publish script  
- Update publish script

1.0.13 @ 29 March 2023  
- Update publish script  
- Fix missing path for calling join  
- Fix missing path for calling join

1.0.12 @ 29 March 2023  
- Packages upgrade and some typo  
- Fix missing path for calling join  
- Packages upgrade and some typo

1.0.11 @ 22 September 2022  
- Packages upgrade and some typo

1.0.10 @ 23 November 2020  
- Fix npm publish command in the publish.sh script file.

1.0.9 @ 23 November 2020  
- Fix git push command in the publish.sh script file.

1.0.8 @ 23 November 2020  
- Change colors of Apply function results.

1.0.7 @ 22 November 2020  
- Add yarn command pub to run publish.sh script

1.0.6 @ 22 November 2020  
- Done with publish.sh script  
- Add option --machine to ```get``` command

1.0.5 @ 22 November 2020  
- Add publish.sh script (alpha)

1.0.4 @ 22 November 2020  
- Update history

1.0.3 @ 22 November 2020  
- Add README with changelog and all functionality  
- Add history and link to bugtracker  
- Fix screenshot, add some yarn commands  
- Fix version  
- Initial commit

1.0.0 @ 22 November 2020  
- Add README with changelog and all functionality

```

## Credits

Contact us: [dev@its.bz](mailto:dev@its.bz)\
Our site: [its.bz](https://its.bz)

## License

MIT
