#!/usr/bin/env node
import {program as commander} from "commander";
import {app, apply, get, inc, init, set} from "./methods.js";


commander
    .name(app.name)
    .version(app.version)
    .description(app.description)

commander.command('init')
    .description('Create config file')
    .action(init)

commander.command('inc <patch|minor|major>')
    .alias('i')
    .description('Increment current version by 1')
    .action(inc)

commander.command('set <version>')
    .alias('s')
    .description('Set version')
    .action(set)

commander.command('get')
    .alias('g')
    .option('--machine', 'Return version only', false)
    .description('Get current version')
    .action(get)

commander.command('apply')
    .alias('a')
    .description('Set current version to all packages')
    .action(apply)

commander.parse(process.argv)
