import chalk from "chalk";
import fs from "fs";
import pkg from "./package.json" assert {type: "json"};
import path from "path";

export const app = pkg

/**
 * Creates new config file
 * If file exist it will be overwritten
 */
export const init = () => {
    const config = fs.readFileSync(path.join(__dirname, '.onever_sample.json'))
    fs.writeFileSync('.onever.json', config)
}

/**
 * Checks is config file exists
 * @returns {boolean}
 */
export const configFileExists = () => {
    if (!fs.existsSync('./.onever.json')) {
        console.log(chalk.red(`\nNo config file found!\nUse ${chalk.yellow(cli_name + ' init')} command to create one.`))
        return false
    }
    return true
}

/**
 * Get current versions
 * @param nul
 */
export const get = (nul) => {
    if (!configFileExists()) return

    try {
        const config = JSON.parse(fs.readFileSync('./.onever.json').toString())
        if (nul.machine) {
            console.log(config.version)
            return 0
        }
        console.log(`Main version is: ${chalk.magenta(config.version)}`)
        let needApply = false
        config.packages.forEach(mod => {
            const modName = JSON.parse(fs.readFileSync(mod).toString())
            if (config.version !== modName.version) needApply = true
            console.log(`Version of ${mod} is: ${config.version !== modName.version ? chalk.red(modName.version) : chalk.green(modName.version)}`)
        })
        if (needApply)
            console.log(chalk.red(`\nOne of the packages versions are differ of the main version.\nUse ${chalk.yellow(cli_name + ' apply')} command to sync it.`))
        else
            console.log(chalk.green('\nNoting to do - All versions are equals'))
    } catch (e) {
        console.error(e)
    }
}

/**
 * Increment by 1 the given version level
 * @param level
 */
export const inc = (level) => {
    if (!configFileExists()) return

    level = level.toLowerCase()
    if (!['major', 'minor', 'patch'].includes(level))
        return console.log(red(`\nInvalid level!\nUse ${yellow(cli_name + ' i major|minor|patch')} command.`))

    try {
        const config = JSON.parse(fs.readFileSync('./.onever.json').toString())
        let ver = config.version.split('.')
        const levels = {'patch': 2, 'minor': 1, 'major': 0}
        ver[levels[level.toLowerCase()]] = (+ver[levels[level.toLowerCase()]] + 1).toString()
        config.version = ver.join('.')
        fs.writeFileSync('./.onever.json', JSON.stringify(config, undefined, 2))
        console.log('New version is: ' + ver.join('.'))
    } catch (e) {
        console.error(e)
    }
}

/**
 * Sets main version
 * @param ver
 */
export const set = (ver) => {
    if (!configFileExists()) return

    try {
        const config = JSON.parse(fs.readFileSync('./.onever.json').toString())
        config.version = ver
        fs.writeFileSync('./.onever.json', JSON.stringify(config, undefined, 2))
        console.log('New version is: ' + ver)
    } catch (e) {
        console.error(e)
    }
}

/**
 * Apply main verson to all packages
 */
export const apply = () => {
    if (!configFileExists()) return

    try {
        const config = JSON.parse(fs.readFileSync('./.onever.json').toString())
        let needApply = 0
        console.log(`Main version is: ${chalk.magenta(config.version)}`)
        config.packages.forEach(mod => {
            const modName = JSON.parse(fs.readFileSync(mod).toString())
            if (config.version !== modName.version)
                needApply++
            const v = modName.version
            modName.version = config.version
            fs.writeFileSync(mod, JSON.stringify(modName, undefined, 2))
            console.log(`Apply version of ${mod}: ${config.version !== v ? chalk(v + ' => ' + config.version) : chalk.green(v)}`)
        })
        if (needApply > 0)
            console.log(chalk.green(`\nThe ${needApply} packages versions were updated.`))
        else
            console.log(chalk.blue('\nAll versions are equal. There are no changes in packages did.'))
    } catch (e) {
        console.error(e)
    }
}